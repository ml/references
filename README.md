Références de base
- Aurélien Géron - Machine Learning avec Scikit Learn
- Chloé-Agathe Azencott: Introduction au Machine Learning (http://cazencott.info/dotclear/public/lectures/IntroML_Azencott.pdf)
- The Elements of Statistical Learning de Hastie, Tibshirani et Friedman (https://web.stanford.edu/~hastie/Papers/ESLII.pdf)

Deep Learning
- Goodfellow: apprentissage profond (https://www.deeplearningbook.org/)
- Livre numérique de deep learning + code: https://d2l.ai/index.html
- Cours de Deep learning:  https://atcold.github.io/pytorch-Deep-Learning/
- Introduction au transport optimal pour la science des données: https://weave.eu/le-transport-optimal-un-couteau-suisse-pour-la-data-science/
- Livre complet: https://arxiv.org/abs/2009.05673 

Apprentissage par renforcement
- Livre de réference: http://incompleteideas.net/book/the-book.html
- Cours de leader de l'équipe de google:  https://deepmind.com/learning-resources/-introduction-reinforcement-learning-david-silver
- Résumé détaillé des algorithmes de renforcement profonds: https://lilianweng.github.io/lil-log/2018/04/08/policy-gradient-algorithms.html

Modèle génératifs:
- Tutoriel technique sur les auto-encodeurs: https://i-systems.github.io/teaching/ML/iNotes/15_Autoencoder.html 

Clustering
- Mélange Gaussien: https://jakevdp.github.io/PythonDataScienceHandbook/05.12-gaussian-mixtures.html

Réduction de dimension
- Cours de apprentissage de variété et apprentissage hyperbolique: https://drewwilimitis.github.io/projects/
- Tutoriel de réduction de dimension: https://blog.paperspace.com/dimension-reduction-with-autoencoders/amp/ 
- Introduction à l'apprentissage géométrique (graphe, variété): https://flawnsontong.medium.com/what-is-geometric-deep-learning-b2adb662d91d

Optimisation
- tutoriel sur le gradient Naturel: https://wiseodd.github.io/techblog/2018/03/14/natural-gradient/

code
- librairie Python simple avec beaucoup d'algorithmes: https://scikit-learn.org/stable/
- librairie Python de deep lmearning: https://keras.io/

Applications
- apprentissage de système hamiltonien:  https://greydanus.github.io/2019/05/15/hamiltonian-nns/
